# README

Diese Notebooks sind motiviert durch das Buch [Automata and Computability: A programmer's perspective](https://www.crcpress.com/Automata-and-Computability-Programmers-Perspective/Gopalakrishnan/p/book/9781138552425) von Ganesh Gopalakrishnan und benutzen insbesondere [Jove](https://github.com/ganeshutah/Jove) = eine Reihe von Python-Modulen, die der Buchautor entwickelt bzw. zusammengestellt hat. 

Ich habe jetzt auch das Verzeichnis `jove` sowie `lex.py` und `yacc.py` hier zur Verfügung gestellt, Lizenzbestimmungen siehe `License.md`. Unten sind minimalen Änderungen am Quellcode angegeben.

Falls Sie Probleme haben damit, Ihre Notebooks "zum Laufen zu bringen", so liegt das vermutlich an Pfad-Problemen. 
So ungefähr sollte Ihr TI-notebooks-Verzeichnis aussehen:

```
ti-notebooks
|
+-- jove (Verzeichnis)
|
+-- lex.py (Datei)
|
+-- yacc.py (Datei)
|
+-- meinallererstesjupyternotebook.ipynb (Datei)
|
+-- Blatt2.ipynb (Datei)
|
...
```

## Unterschiede zum Original-Jove-Quellcode

Output von `diff jove/DotBashers.py jove/DotBashers_orig.py`:

```
jovyan@24438f0668e2:~/TI-notebooks$ diff jove/DotBashers.py jove/DotBashers_orig.py
439c439
<     """Show Epsilons as '&epsilon;' in printouts if visible_eps
---
>     """Show Epsilons as '@' in printouts if visible_eps
445c445
<             return '&epsilon;'
---
>             return '@'
607c607
<     return [(a, reduce(lambda x,y: x + "," + y, b)) for (a,b) in d.items()]  # concat the ys now
---
>     return [(a, reduce(lambda x,y: x + " \n " + y, b)) for (a,b) in d.items()]  # concat the ys now
```

## NEU

PDF-Versionen der Notebooks (mit allen Ausgaben und allen Grafiken) finden Sie im Verzeichnis pdf.
Das Verzeichnis wird ab und zu aktualisiert.