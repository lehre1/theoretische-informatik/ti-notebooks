# damm@informatik.uni-goettingen.de
# 2019

"""
Grammatik-Modul im Jove-Style. Es fehlt noch eine Menge, z.B.
- cfgrammar2ChNF(G)
- generated_by_csgrammar(G,w)
- rlderivation(G,w), cfderivation(G,w), syntaxtree(G,w)
- cfgrammar2PDA(G), chgrammar2TM(G)
- ...

TODOs:
- Test auf epsilon-Freiheit (bzw. Sonderregel S->epsilon)
- md2grammar analog zu md2mc
- pda2cfgrammar
- tm2chgrammar
- ...
"""

# from jove.LangDef import *
from jove.DotBashers import is_consistent_dfa

def dfa2rlgrammar(D,string_based = False):
    '''returns right-linear grammar that is language equivalent to DFA D
    optional parameter determines format of the rules l->r:
    - string based:          (l,r) = (<string>,<string>) or 
    - tuple based (default): (l,r) = (<tuple of strings>,<tuple of strings>)
    [String based sounds natural and convenient, but is restricted to length
    1 strings (characters), as it can otherwise cause trouble. 
    Example: (V,Sigma,P,S) = ({S,Sb,T},{a,b},{ ...},S).  
    How to distinguish Nonterminal Sb from concatenated S.b?]

    '''
    assert(is_consistent_dfa(D)), str(D)+": is not a consistent DFA"
    (V,Sigma,S,delta,F) = (D['Q'],D['Sigma'],D['q0'],D['Delta'],D['F'])
    states_message ='''
    string based format can handle only single upper-case characters as state names
    (optional parameter string_based=False forces tuple based format)'''
    symbols_message ='''
    string based format can handle only single lower-case characters or digits as symbols
    (optional parameter string_based=False forces tuple based format)'''
    if string_based:
        for q in V:
            assert((len(q) == 1 and q.isupper())),str(q)+": "+ states_message
        for sigma in Sigma:
            assert(len(sigma) == 1 and (sigma.islower() or sigma.isdigit())),str(sigma)+": "+ symbols_message        
    P = set()
    for q in V:
        for sigma in Sigma:
            rule = (q,sigma + str(delta[q,sigma])) if string_based else ((q,),(sigma,delta[q,sigma]))
            P.add(rule)
            if delta[q,sigma] in F:
                rule = (q,sigma) if string_based else ((q,),(sigma,))
                P.add(rule)
    G = {'V': V ,'Sigma': Sigma, 'P': P, 'S': S}
    return G

def grammar_format(G):
    '''
    if G is a consistent grammar, it's format is returned as a string 
    '''
    (V,Sigma,P,S) = (G['V'],G['Sigma'],G['P'],G['S'])
    string_based = type(list(P)[0][0]) == str # if True, all rules should be given as (<string>,<string>)
    for p in P: 
        assert(type(p) == tuple and len(p) == 2), str(p)+": not a production"
        (l,r) = p
        if string_based:
            assert(type(l) == type(r) == str), str(p)+": wrong rule format (maybe string_based=False?)"
            assert(len(l) > 0), str(p)+": empty left side forbidden"
            for s in l:
                assert(s in Sigma | V), str(l)+" has wrong symbols"
            for s in r:
                assert(s in Sigma | V), str(r)+" has wrong symbols"
            format = 'string'        
        else:
            assert(type(l) == type(r) == tuple), str(p)+": wrong rule format (maybe string_based=True?)"
            assert(len(l) > 0), str(p)+": empty left side forbidden"
            for s in l:
                assert(type(s) == str and set(w) <= Sigma | V), str(l)+" has wrong parts"
            for s in r:
                assert(type(s) == str and set(w) <= Sigma | V), str(r)+" has wrong parts"
            format = 'tuple'    
    return format

def grammartype(G):
    # TODO: Code vereinfachen, Funktion grammar_format benutzen
    # TODO: epsilon-Sonderregel prüfen!
    '''
    returns Chomsky-type of given grammar G
    '''
    assert(type(G) == dict), "wrong type"
    assert(set(G.keys()) == {'V','Sigma','P','S'}), "wrong keys"
    (V,Sigma,P,S) = (G['V'],G['Sigma'],G['P'],G['S'])
    assert((type(V) == type(Sigma) == type(P) == set) 
           and V & Sigma == set({}) and type(S) == str), "wrong structure"
    assert(S in V), "start symbol not in variables"
    for N in V: 
        assert(type(N) == str), str(N)+": no valid variable (not a string)"       
    assert(Sigma != set()), "empty alphabet"
    for sigma in Sigma: 
        assert(type(sigma) == str), str(sigma)+": no valid symbol (not a string)"
    assert(P != set()), "no productions"

    string_based = type(list(G['P'])[0][0]) == str # if True, all rules should be given as (<string>,<string>)
    for p in G['P']: 
        assert(type(p) == tuple and len(p) == 2), str(p)+": not a production"
        (l,r) = p
        if string_based:
            assert(type(l) == type(r) == str), str(p)+": wrong rule format (maybe string_based=False?)"
            assert(len(l) > 0), str(p)+": empty left side forbidden"
            for s in l:
                assert(s in Sigma | V), str(l)+" has wrong symbols"
            for s in r:
                assert(s in Sigma | V), str(r)+" has wrong symbols"
        else:
            assert(type(l) == type(r) == tuple), str(p)+": wrong rule format (maybe string_based=True?)"
            assert(len(l) > 0), str(p)+": empty left side forbidden"
            for s in l:
                assert(type(s) == str and set(s) <= Sigma | V), str(l)+" has wrong parts"
            for s in r:
                assert(type(s) == str and set(s) <= Sigma | V), str(r)+" has wrong parts"
    t = 0
    for (l,r) in P:
        if len(l)>len(r): return t
        if len(r) == 1 and r[0] == '':
            if l != [S]: return t
            for (l_,r_) in P:
                for part in r_:
                    if S in part: return t
    t = 1
    for (l,r) in P:
        if len(l)>1 or l[0] not in V: return t
    t = 2    
    for (l,r) in P:
        if len(r) > 2: return t
        if len(r) <= 2 and r[0] not in Sigma: return t
        if len(r) == 2 and r[1] not in V: return t
    t = 3
    return t

def rlgrammar2nfa(G):
    '''
    returns NFA, that is language equivalent to given right-linear grammar G
    '''
    assert(grammartype(G) == 3), "not a right-linear grammar"
    assert('X' not in G['V']), "reserved 'X' forbidden as a nonterminal"
    (Q,Sigma,Q0,F) = (G['V']|{'X'},G['Sigma'],{G['S']},{'X'})
    P = G['P']
    Delta = dict()
    for q in Q:
        for sigma in Sigma:
            Delta[(q,sigma)] = set()
    for (l,r) in P:
        if len(r) == 1: # r is a terminal symbol
            Delta[(l[0],r[0])] = Delta[(l[0],r[0])] | {'X'}
        else: # r consists of a terminal and a nonterminal
            Delta[(l[0],r[0])] = Delta[(l[0],r[0])] | {r[1]}
    N = dict()
    (N['Q'],N['Sigma'],N['Delta'],N['Q0'],N['F']) = (Q,Sigma,Delta,Q0,F)
    return N

def is_ChNF(G,chatty=False):
    '''
    checks whether G is in Chomsky normal form (explaining errors if chatty==True)
    '''
    assert(grammartype(G)>=2 and type(list(G['P'])[0][0]) != str), " not a CFG in tuple format"
    (V,Sigma,P) = (G['V'],G['Sigma'],G['P'])
    for (l,r) in P:
        if len(r)>2:
            if chatty: print(str((l,r))+': right side too long')
            return False
        elif (len(r)==2 and not {r[0],r[1]}.issubset(V)):
            if chatty: print(str((l,r))+': terminal symbol in right side')
            return False
        elif (len(r)==1 and r[0] in V):
            if chatty: print(str((l,r))+': is a chain rule')
            return False
        elif (len(r)==0):
            if chatty: print(str((l,r))+': is an epsilon-rule')
        else: pass
    return True

def cyk_table(G,x,chatty=False):
    '''
    returns CYK table (as dict) of parsing x with G
    if chatty: additionally prints the table
    ''' 
    # format checking -------------------------------------------------------
    assert(is_ChNF(G)), str(G)+": not a ChNF grammar"
    assert(type(x)== str), str(x)+": not a string"
    (V,Sigma,P,S) = (G['V'],G['Sigma'],G['P'],G['S'])
    n = len(x)
    if n == 0: return False
    for i in range(n):           # for i = 0, 1, ..., n-1
        assert(x[i] in Sigma), str(x[i])+": wrong symbol"
        
    # CYK algorithm ========================================================
    P1 = set((l,r) for (l,r) in P if len(r) == 1)  # rules of shape A -> a 
    P2 = set((l,r) for (l,r) in P if len(r) == 2)  # rules of shape A -> BC
    t = dict()
    j = 1                            # start with subwords x[i:i+1] = x[i] of length j == 1
    # t[i,1] = set()
    for i in range(n):               # their possible start positions
        t[i,1] = set(A for ((A,),(a,)) in P1 if a == x[i])
    # ----------------------------------------------------------------------    
    for j in range(2,n+1):           # now consider subwords x[i,i+j] = x[i]...x[i+j-1] of lengths j = 2..n
        for i in range(n-j+1):       # their start positions are i = 0, 1, ..., n-j 
            t[i,j] = set({})
            for k in range(1,j):     # possible split lengths are k = 1, 2, ..., j-1
                t[i,j].update({A for ((A,),(B,C)) in P2 if (B in t[i,k] and C in t[i+k,j-k])})   
    # ----------------------------------------------------------------------    

    if chatty:
        w = max(max(len(t[i,j]) for j in range(1,n+1-i)) for i in range(n)) # width of a column
        print('+'+('-'*w +'+')*n)
        print('|',end='')                                  # end='' prevents trailing newline
        for i in range(n): print(' '*(w-1)+str(x[i])+'|',end='')
        print('\n+'+('-'*w+'+')*n)    
        for j in range(1,n+1):
            print('|',end='')        
            for i in range(n-j+1):
                size = len(t[i,j])
                if size == 0: print(' '*w,end='')
                else: 
                    print(' '*(w-size),end='')
                    for v in t[i,j]: print(v,end='')
                print('|',end='')
            print('\n+'+('-'*w+'+')*(n-j+1))
    return t                

def generated_by_rlgrammar(G,w):
    '''
    checks whether right-linear grammar G generates w
    '''
    N = rlgrammar2nfa(G)
    return accepts_nfa(N,w)

def generated_by_cfgrammar(G,w,chatty=False):
    '''
    checks whether context free grammar G generates w 
    if chatty: additionally prints CYK-table
    '''
    table = cyk_table(G,w,chatty)
    return G['S'] in table[0,len(w)]

def derive(v,G):
    '''
    returns set of words directly derivable from v using G's rules
    '''
    assert(grammar_format(G) == 'string'), "grammar not in string format"
    import re
    D = set()
    for p in G['P']:
        (l,r) = (p[0],p[1])
        matches = [m.start() for m in re.finditer(l,v)]
        for m in matches:
            D.add(v[:m] + r + v[(m+len(l)):])         
    return D 

def ABL(G,n,X):
    A = set()
    for v in X: 
        for w in derive(v,G):
            if len(w) <= n: A.add(w)
    return A

def generated_by_csgrammar(G,x):
    '''
    checks whether context sensitive grammar G generates w 
    '''
    assert(grammartype(G) >= 1), "not a context-sensitive grammar"
    (Z,X) = (set(),{G['S']})  # leere Menge bzw. { Startsymbol }
    n = len(x)
    while not x in X and X != Z:
        (Z,X) = (X,ABL(G,n,X))
    return x in X    

def ABLunbd(G,X):
    A = set()
    for v in X: 
        for w in derive(v,G):
            A.add(w)
    return A

def generated_by_grammar(G,x,chatty=False):
    '''
    checks whether Chomsky grammar G generates w 
    if chatty: 
    additionally prints for each iteration the maximum sentence length encountered so far
    '''
    assert(grammartype(G) >= 0), "not a Chomsky-grammar"
    X = {G['S']}  
    if chatty:
        print("maximum sentence lengths encountered so far:")
        n = 0
    while not x in X:
        X = ABLunbd(G,X)
        if chatty: print(max([len(s) for s in X]))
    return True    

print('''You may use any of these help commands:
help(derive)
help(grammartype)
help(rlgrammar2nfa)
help(dfa2rlgrammar)
help(is_ChNF)
help(cyk_table)
help(generated_by_rlgrammar)
help(generated_by_cfgrammar)
help(generated_by_csgrammar)
help(generated_by_grammar)
''')
